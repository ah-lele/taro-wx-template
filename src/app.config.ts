export default defineAppConfig({
  pages: [
    "pages/home/index",
    "pages/cinema/index",
    "pages/my/index",
    "pages/order/index",
    "pages/index/index",
    "pages/cinema-detail/index",
    "pages/movie-detail/index",
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black",
  },
  tabBar: {
    custom: true,
    color: "#858a99",
    selectedColor: "#DC143C",
    backgroundColor: "#ffffff",
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
      },
      {
        pagePath: "pages/cinema/index",
        text: "影院",
      },
      {
        pagePath: "pages/order/index",
        text: "订单",
      },
      {
        pagePath: "pages/my/index",
        text: "我的",
      },
    ],
  },
  permission: {
    "scope.userLocation": {
      desc: "帮助您找到附近的影院",
    },
  },
});
