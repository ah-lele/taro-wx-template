import { defineStore } from "pinia";
import { store } from "../index";

export const useTabbarStore = defineStore("tabbar", {
  state: () => ({
    selected: 0,
    bottomDistance: 0,
    tabbarHeight: 0,
    orderSelected: "",
    orderTabsIndex: 0,
  }),
  getters: {
    getSelected(state) {
      return state.selected;
    },
    getBottomDistance(state) {
      return state.bottomDistance;
    },
  },
  actions: {
    setSelected(value) {
      this.selected = value;
    },
    setBottomDistance(value) {
      this.bottomDistance = value;
    },
    setTabbarHeight(value) {
      this.tabbarHeight = value;
    },
  },
});

export function getTabbarStore() {
  return useTabbarStore(store);
}
