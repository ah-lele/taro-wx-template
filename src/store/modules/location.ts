import { defineStore } from "pinia";
import { store } from "../index";
import { getStorage, setStorage } from "../../utils/tools";
// import * as cityApi from "../../api/city";

export const useLocationStore = defineStore("location", {
  state: () => ({
    allCitydata: [],
    hotCityData: [],
    latitude: getStorage("location_latitude") || null,
    longitude: getStorage("location_longitude") || null,
    currentCityinfo: getStorage("location_currentCityinfo") || null,
    selectCityinfo: getStorage("location_selectCityinfo") || {
      id: 130100,
      cityName: "石家庄市",
    },
  }),
  getters: {
    getSelectCityinfo() {
      if (this.selectCityinfo) {
        return this.selectCityinfo;
      } else {
        return this.currentCityinfo;
      }
    },
    getCitylatlng() {
      if (
        this.currentCityinfo &&
        this.selectCityinfo.cityName.includes(this.currentCityinfo.cityName)
      ) {
        return [this.latitude, this.longitude];
      } else {
        return [0, 0];
      }
    },
  },
  actions: {
    setSelectCityinfo(data) {
      this.selectCityinfo = data;
      setStorage("location_selectCityinfo", this.selectCityinfo);
    },
  },
});

export function getLocationStore() {
  return useLocationStore(store);
}
