import { defineStore } from "pinia";
import { setStorage, getStorage, clearStorage } from "../../utils/tools";
import { store } from "../index";

export const useUserStore = defineStore("user", {
  state: () => ({
    Token: getStorage("TOKEN") || null,
    Userinfo: getStorage("userInfo") || null,
  }),
  getters: {},
  actions: {
    setToken(token: string) {
      this.Token = token;
      setStorage("TOKEN", token);
    },

    setUserinfo(userinfo: Object) {
      this.Userinfo = userinfo;
      setStorage("userInfo", userinfo);
    },
    loggout() {
      this.Token = "";
      this.Userinfo = "";
      clearStorage("TOKEN");
      clearStorage("userInfo");
    },
    async getUserinfo() {},
  },
});

export function getUserStore() {
  return useUserStore(store);
}
