import { createPinia } from "pinia";
const store = createPinia();

export { store };

export * from "./modules/user";
export * from "./modules/tabbar";
export * from "./modules/location";

export default store;
