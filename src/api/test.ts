import requestService from "../service/marketRequest";

const Api = {
  list: "/autoReply/getReplyList",
  del: "/autoReply/deleteReplyInfo",
  add: "/autoReply/addAutoReply",
};

//获取当前的回复列表
export function answerList(data) {
  return requestService(Api.list, {
    type: "post",
    data,
    // paramsFormdata: "application/x-www-form-urlencoded",
  });
}
//删除自动回复 列表的某一项
export function delAnswer(data) {
  return requestService(Api.del, {
    type: "post",
    data,
    paramsFormdata: "application/x-www-form-urlencoded",
    // paramsFormdata: "application/x-www-form-urlencoded",
  });
}
//添加自动回复
export function addAnseer(data) {
  return requestService(Api.add, {
    type: "post",
    data,
  });
}
