import { createApp } from "vue";
import { createPinia } from "pinia";
import Taro from "@tarojs/taro";

const App = createApp({
  mounted() {},
  onLaunch(option) {},
  onShow() {},
  onHide() {},
});
App.use(createPinia());
export default App;
