import Taro from "@tarojs/taro";
// import pinyin from "pinyin";
export const setStorage = (key: any, value: any) => {
  return Taro.setStorageSync(key, value);
};

export const getStorage = (key: any) => {
  return Taro.getStorageSync(key);
};

export const clearStorage = (key: any) => {
  return Taro.removeStorageSync(key);
};

export const getCurrentPageUrlWithArgs = () => {
  //获取带参链接
  const pages = Taro.getCurrentPages();
  const currentPage = pages[pages.length - 1];
  const url = currentPage.route;
  const options = currentPage.options;
  let urlWithArgs = `/${url}?`;
  for (let key in options) {
    const value = options[key];
    urlWithArgs += `${key}=${value}&`;
  }
  urlWithArgs = urlWithArgs.substring(0, urlWithArgs.length - 1);
  return urlWithArgs;
};

//日期字符串转成时间戳
//例如var date = '2015-03-05 17:59:00.0';
export const dateStrChangeTimeTamp = (dateStr) => {
  dateStr = dateStr.substring(0, 19);
  dateStr = dateStr.replace(/-/g, "/");
  return new Date(dateStr).getTime();
};

//时间戳转为日期，年月日
export const timestampToTimeYMD = (timestamp) => {
  const date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
  const Y = date.getFullYear() + "年";
  const M =
    (date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1) + "月";
  const D = date.getDate() + "日";
  return Y + M + D;
};
// 对数组进行分类
export const arrayGroupBy = (objectArray, property) => {
  return objectArray.reduce(function (acc, obj) {
    let key = obj[property];
    if (!acc[key]) {
      acc[key] = [];
    }
    acc[key].push(obj);
    return acc;
  }, {});
};

//防抖函数
export const debounce = (fn, delay = 500) => {
  const timer = null;
  return (...args) => {
    if (timer !== null) {
      clearTimeout(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, args);
      timer = null;
    }, delay);
  };
};

 

export const transformDataToNewFormat = (data) => {
  console.log(data, "data");

  const newData = [];

  // 创建包含 A 到 Z 的字母的数组
  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

  // 初始化新数据结构
  alphabet.forEach((letter) => {
    newData.push({
      title: letter,
      list: [],
    });
  });

  // 将城市数据分组到对应的字母标题下
  data.forEach((city) => {
    const firstLetter = city.cityName[0].toUpperCase();

    let pinyinFirstLetter;

    // 使用条件语句，如果城市名是 "重庆"，则将首字母设置为 "C"
    if (city.cityName.indexOf("重庆") !== -1) {
      pinyinFirstLetter = "C";
    } else {
      const pinyinArray = pinyin(firstLetter, {
        style: pinyin.STYLE_NORMAL, // 使用普通风格的拼音
      });
      pinyinFirstLetter =
        pinyinArray.length > 0
          ? pinyinArray[0][0][0].toUpperCase()
          : firstLetter;
    }

    const index = alphabet.indexOf(pinyinFirstLetter);

    if (index !== -1) {
      newData[index].list.push({ id: city.id, name: city.cityName });
    }
  });

  return newData.filter((item) => item.list.length > 0);
};
