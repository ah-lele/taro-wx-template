import { useUserStore } from "../store";
import Taro from "@tarojs/taro";
import { getCurrentPageUrlWithArgs } from "./tools";

export const needLogin = {
    //mixin的方式
    onShow() {
        const userStore = useUserStore();
        const url = getCurrentPageUrlWithArgs(); //当前带参数的路径
        if (!userStore.Token) {
            Taro.redirectTo({
                url: `/pages/login/index?url=${encodeURIComponent(url)}`,
            });
        }
    },
};
